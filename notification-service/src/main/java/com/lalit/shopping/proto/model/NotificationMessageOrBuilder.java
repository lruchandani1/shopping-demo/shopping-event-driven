// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: notification/notification.proto

package com.lalit.shopping.proto.model;

public interface NotificationMessageOrBuilder extends
    // @@protoc_insertion_point(interface_extends:NotificationMessage)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string firstName = 1;</code>
   * @return The firstName.
   */
  java.lang.String getFirstName();
  /**
   * <code>string firstName = 1;</code>
   * @return The bytes for firstName.
   */
  com.google.protobuf.ByteString
      getFirstNameBytes();

  /**
   * <code>string lastName = 2;</code>
   * @return The lastName.
   */
  java.lang.String getLastName();
  /**
   * <code>string lastName = 2;</code>
   * @return The bytes for lastName.
   */
  com.google.protobuf.ByteString
      getLastNameBytes();

  /**
   * <code>string emailAddress = 3;</code>
   * @return The emailAddress.
   */
  java.lang.String getEmailAddress();
  /**
   * <code>string emailAddress = 3;</code>
   * @return The bytes for emailAddress.
   */
  com.google.protobuf.ByteString
      getEmailAddressBytes();

  /**
   * <code>repeated .NotificationMessage.PhoneNumber phoneNumbers = 5;</code>
   */
  java.util.List<com.lalit.shopping.proto.model.NotificationMessage.PhoneNumber> 
      getPhoneNumbersList();
  /**
   * <code>repeated .NotificationMessage.PhoneNumber phoneNumbers = 5;</code>
   */
  com.lalit.shopping.proto.model.NotificationMessage.PhoneNumber getPhoneNumbers(int index);
  /**
   * <code>repeated .NotificationMessage.PhoneNumber phoneNumbers = 5;</code>
   */
  int getPhoneNumbersCount();
  /**
   * <code>repeated .NotificationMessage.PhoneNumber phoneNumbers = 5;</code>
   */
  java.util.List<? extends com.lalit.shopping.proto.model.NotificationMessage.PhoneNumberOrBuilder> 
      getPhoneNumbersOrBuilderList();
  /**
   * <code>repeated .NotificationMessage.PhoneNumber phoneNumbers = 5;</code>
   */
  com.lalit.shopping.proto.model.NotificationMessage.PhoneNumberOrBuilder getPhoneNumbersOrBuilder(
      int index);

  /**
   * <code>.NotificationMessage.Order order = 6;</code>
   * @return Whether the order field is set.
   */
  boolean hasOrder();
  /**
   * <code>.NotificationMessage.Order order = 6;</code>
   * @return The order.
   */
  com.lalit.shopping.proto.model.NotificationMessage.Order getOrder();
  /**
   * <code>.NotificationMessage.Order order = 6;</code>
   */
  com.lalit.shopping.proto.model.NotificationMessage.OrderOrBuilder getOrderOrBuilder();
}
