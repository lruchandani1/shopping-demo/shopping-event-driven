package com.lalit.shopping.notification.web;

import lombok.extern.slf4j.Slf4j;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

import com.lalit.shopping.proto.model.NotificationMessage;

@RestController
@Slf4j
public class NotificationController {

  @PostMapping(value = "/notification")
  public Mono<?> notification(@RequestBody NotificationMessage notificationMessage){
    return Mono.from(ResponseEntity::ok)
        .doFirst(()->log.info("Notification : {}",notificationMessage.getFirstName()));
  }
}
